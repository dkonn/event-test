<?php
/* @var $this yii\web\View
 * @var $msg string
 */

use app\controllers\SomeController;
?>

<p>Before trigger content:</p>
<?php Yii::$app->controller->trigger(SomeController::EVENT_BEFORE_INDEX) ?>

<p>This is some page: <?= $msg ?></p>

<p>After trigger content:</p>
<?php Yii::$app->controller->trigger(SomeController::EVENT_AFTER_INDEX) ?>