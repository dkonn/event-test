<?php

namespace app\hooks;

use Yii;
use app\controllers\SomeController;
use yii\base\Event;
use yii\web\Controller;

class SomeControllerHooks
{
    /** @var Controller SomeController */
    public $controller;

    public function __construct(SomeController $controller)
    {
        $this->controller = $controller;
        $this->initEvents();
    }

    protected function initEvents()
    {
        $this->controller->on(SomeController::EVENT_BEFORE_INDEX, function (Event $event) {
            echo 'this is default before';
        });

        $this->controller->on(SomeController::EVENT_AFTER_INDEX, function (Event $event) {
            echo 'this is default after';
        });
    }

    public function index($data)
    {
        return $this->controller->render('index', [
            'msg' => 'Default: ' . $data,
        ]);
    }
}

