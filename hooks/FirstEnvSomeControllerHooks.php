<?php

namespace app\hooks;

use app\controllers\SomeController;
use yii\base\Event;

class FirstEnvSomeControllerHooks extends SomeControllerHooks
{
    protected function initEvents()
    {
        $this->controller->on(SomeController::EVENT_BEFORE_INDEX, function (Event $event) {
            echo 'this is modified before';
        });

        $this->controller->on(SomeController::EVENT_AFTER_INDEX, function (Event $event) {
            echo 'this is modified after';
        });
    }

    public function index($data)
    {
        return $this->controller->render('index', [
            'msg' => 'First env modified: ' . $data,
        ]);
    }
}