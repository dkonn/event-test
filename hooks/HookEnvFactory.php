<?php

namespace app\hooks;

use app\controllers\SomeController;
use yii\web\Controller;

class HookEnvFactory
{
    const FIRST_ENV = 'FirstEnv';
    const SECOND_ENV = 'SecondEnv';

    const SOME_CONTROLLER_TYPE = SomeController::class;

    public function create(Controller $controller, ?string $env = '')
    {
        $controllerClass = explode('\\', get_class($controller));
        $controllerClass = end($controllerClass);
        $hooksClass = '\\app\\hooks\\' . $env . $controllerClass . 'Hooks';
        return new $hooksClass($controller);
    }
}