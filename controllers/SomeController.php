<?php

namespace app\controllers;

use app\hooks\HookEnvFactory;
use app\hooks\SomeControllerHooks;
use yii\web\Controller;

class SomeController extends Controller
{
    const EVENT_BEFORE_INDEX = 'someControllerBeforeIndex';
    const EVENT_AFTER_INDEX = 'someControllerAfterIndex';

    /** @var SomeControllerHooks $hooks */
    public $hooks;

    public function init()
    {
        //initialize hooks
        // with docker env var - $env = getenv('ENV_TYPE');
        $env = HookEnvFactory::FIRST_ENV;
        $this->hooks = (new HookEnvFactory())->create($this, $env);
        parent::init();
    }

    public function actionIndex()
    {
        return $this->hooks->index('some request data');
    }
}